# Importing a Project into Git

Assuming an existing project directory named **git-bare-sample**

```
cd git-bare-sample
git init
git remote add origin git@gitlab.com:netskink/git-bare-import-sample.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

# Submodules

In this git repo git-testy change directory to the root and do this to add as a submodule.

```
git submodule add git@gitlab.com:netskink/git-bare-import-sample.git
git status
git commit -am "add git-bare-import-sample"
git push origin master
```

## Cloning a git repo with submodules

```
git clone git@gitlab.com:netskink/git-testy.git
```

Now the submodule will have a directory but no contents.

You must run two commands: 

* git submodule init to initialize your local configuration file, 
* git submodule update to fetch all the data from that project and check out the appropriate commit listed in your superproject:

```
cd git-bare-import
git submodule init
git submodule update
```

Make a change to submodule and push to origin

```
vi README.md
git add README.md
git status
git commit -m "wip"
git push
git push origin HEAD:master
```

You can also clone with submodule command.


```
git clone --recurse-submodules git@gitlab.com:netskink/git-testy.git
```

If you already cloned the project and forgot --recurse-submodules, you can combine the git submodule init and git submodule update steps by running git submodule update --init. To also initialize, fetch and checkout any nested submodules, you can use the foolproof git submodule update --init --recursive.

```
git submodule update --init --recursive
```



  
