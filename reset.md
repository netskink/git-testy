# reset

## unstage a specific file

```
$ git reset file_name
```


# soft

```
$ git reset --soft HEAD^
```

Backs out changes, but keeps changes in staging.


## mixed

```
$ git reset --mixed HEAD^
```

Default if --soft or --hard not specified.  Removes commit and
also from staging.  Changes are kept in filesystem.  Its like
you haven't done the git add step yet.

## hard

```
$ git reset --hard HEAD^
```

Resets to original state: -no files in staging, no changes in file system.

