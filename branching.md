# git branching and merging

## Tags

Use `git tag -l` to list tags of the repo

Use `git tag <tag_name> <commit_ref>` to tag a particular commit

Use `git push --tags <remote>` to push the tags

What are these images?
![git image](file:///C:/Users/Owner/Downloads/03.svg)

![git image](https://images.app.goo.gl/iVEs35FJg3Uye4Qq6)

## Creating a new branch

Examine these commands, but look at the summary example at the bottom for the quick-guide.

```
git branch newbranchname        <--- creates a new local branch
git checkout newbranchname      <--- checks out the new branch
or
git checkout -t newbranchname   <--- checks out the branch and enables tracking.  Does this omit push -u rqmt?
```

or to do in one step

```
git checkout -b newbranchname
```

afterwards, you do your 

```
git add
git commit 
```

When ready to push do this

```
# remote is required?
# branch name is optional
git push <remote> <branchname> 

git push origin newbranchname
git push --set-upstream origin testy
```

### Complete example

This example walkthrough will create a new dir and file in a
new branch.  Push the branch to the server and then leave
the user in the new branch.

```
git branch JFD
git checkout JFD
mkdir JFD_DIR
echo "JFD Branch" > JFD_DIR/jfd_file.txt
git add JFD_DIR/jfd_file.txt
git commit -m "adding a jfd dir and file in JFD branch"
git push -u origin JFD
```
## Pushing to a remote

Some notes on pushing to a remote

```
# This pushes but does not set origin?  Why use it?
git push origin JFD

# These two are equivalent
git push --set-upstream origin JFD
git push -u origin JFD
```

## Adding a remote
git remote add origin JFD

## Listing branches

List local branches

```
git branch
```

List remote branches

```
git branch --remote
```

List all branches local and remote

```
git branch --all
```



## Switching Branches

The original method was to use checkout

```
git checkout branchname
or
git co branchname
```

The new method is to use switch (git 2.23)

```
git switch branchname
```

Some stackoverflow post showed this

```
git branch                   ->  lists all branches
git checkout "branchname"    ->  switches to your branch
git push origin "branchname" ->  Pushes to your branch
git add */filename           -> Stages *(All files) or by given file name
git commit -m "commit message" -> Commits staged files
git push                     -> Pushes to your current branch

```


