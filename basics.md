# Git basics

Its helpful when using git to have your keys cached for ssh so that you can do git commands
without having to typing your username and password or passphrase each time.


## Methods for obtaining keys


### Windows method to obtain keys

use notepad to open c:\users\"login name"\.ssh\id_rsa.put
Copy the entire text to obtain public key.

Talk about pagent, pgenerate.  Sadly for the windows computer we used, this did not reflect the ssh keys used by the git terminal.  Need to expand this.  We used pgenerate to import the private key and then exported the private key to make the .ppk file.  We then imported this key into pagent.  We restarted the git bash terminal but it did not recoginize the key in pagent.

### Linux method to create keys which specify name and passphrase

```
# ssh-keygen -t rsa -N _put_passphrase_here_ -C _comment_ -f _output_file_
ssh-keygen -t rsa -N _put_passphrase_here -C _put_comment_here_ -f id_rsa_something_here_
xclip -sel clip < id_rsa_something_here_.pub

```

### OSX method to create keys

```
ssh-keygen -t rsa -b 4096
pbcopy < ~/.ssh/id_rsa.pub
```

### Use config file to use different keys for differnt sites

```
                                            These lines can be anything
                                            The ssh key is all that matters.
                                            Note the git clone modification though.
                                                   |
cat ~/.ssh/config                                  |
# company specific github                    <-----+
Host github.com-company                      <-----+
    HostName github.com                            |
    User git                                       |
    IdentityFile ~/.ssh/id_rsa_company             |
    # modify any git clone url to be of this       |
    # form:                                        |
    # git@github.com-company:Company/some_repo.git |
                                                   |
# default github                             <-----+
Host github.com                              <-----+
    HostName github.com              
    User git 
    IdentityFile ~/.ssh/id_rsa


```

Use as follows:

```
git clone git@github.com-cinnamon:Cinnamon/lib-preprocessing.git
```


From some comment section.  The guy does similar to us, but specifies a paramter.  Is he trying to specify an output dir for the repo?  I believe so.

```
Instead of having to change the remote origin in the config after cloning, you can just use that custom Host variable on the git clone command instead.

clone your repo git clone git@github.com:activehacker/gfs.git gfs_jexchan

e.g.

git clone git@github.com-activehacker:activehacker/gfs.git gfs_jexchan


as oppose to:

git clone git@github.com:activehacker/gfs.git gfs_jexchan

that's the ticket!
```



## Basic Git Commands

In these commands angle brackets are used to denote required parameters and square brackets denote optional parameters.


### git status

Provides details on modified, deleted and new files. Untracked means file is not under Git control. Tracked, Modified means under Git control and has been modified. Staged means file will be pushed, see git add/commit.

```
git status 
```


### `git add`

Stages file for commit. Git status will show file as staged for commit.

```
git Add <filename> 
```

### git commit

Specifies the message for git commit.

```
git commit -m "message here"
```


### git fetch

Updates the git metadata to show status of the repo. Now git status will show the repo is out of date.

```
git fetch 
```


### git pull

Actually pulls the newly added files from the remote git server to local git repo.

```
git pull
```


### git log

Displays the commit history of a branch.

```
git log 
```


### git diff


Displays detailed changes (and line numbers) of files between commits identified by hash1 and hash2. The full text of hash1 and hash2 do not need to be input. 

```
git diff <hash1> <hash2> 
```



### git show

Displays the details to files (and line numbers) of the commit identified by hash1.

```
git show <hash1> 
```

better
```
git diff-tree --no-commit-id --name-only -r bd61ad98
```



### git mv

Renames filename1 to filename2 in the same directory location in bash and git.

```
git mv <filename1> <filename2> 
```



### Deleting a file from git

* Method 1: 

```
rm <filename>       removes <filename> from local filesystem
git add <filename>  stages file to be removed from repo
```

* Method 2

Removes <filename> from local filesystem and stages change

```
git rm <filename> 

```


### git commit

Retrieving a specific version of a file from repo


```
git checkout <specific revision hash> -- <filename>

```



###  .gitignore

add a text file called .gitignore
In this file, put file patterns to ignore.

#### Example

```
*.tmp
data/*

```



