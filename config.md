# notes on git config

## Get remotes

```
$ git config --get remote.origin.url
```

Can also do via:

```
$ git remote -v
```

Can also do via:

```
$ cat .git/config | grep remote
```

# medium article on adding alias

* [url](https://medium.com/@lordmoma/so-you-think-you-know-git-673f9c4b0792)

Contents of .gitconfig

```
[alias]
 graph = log --oneline --graph --decorate
 ls = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate
 ll = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat
 lds = log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short
 conflicts = diff --name-only --diff-filter=U
 local-branches = !git branch -vv | cut -c 3- | awk '$3 !~/\\[/ { print $1 }'
 recent-branches = !git branch --sort=-committerdate | head
 authors = !git log --format='%aN <%aE>' | grep -v 'users.noreply.github.com' | sort -u --ignore-case
 search = "!f() { git rev-list --all | xargs git grep -F \"$1\"; }; f"
 dl = "!git ll -1" # Show modified files in last commit:latest commit
 dlc = diff --cached HEAD^ # Show modified files in last commit:latest commit
 dr  = "!f() { git diff "$1"^.."$1"; }; f" # git dr <commit-id>
 lc  = "!f() { git ll "$1"^.."$1"; }; f" # git lc <commit-id> # show modified files in <commit-id>
 f = "!git ls-files | grep -i" # git f <filename> # search <filename> in all files
 alias = ! git config --get-regexp ^alias\\. | sed -e 's/^alias\\.\\([^ ]*\\) \\(.*\\)$/\\x1b[36m\\1\\x1b[0m=\\x1b[35m\\2\\x1b[0m/'

# these two don't work, he didn't post his scripts
# bb = !/Users/davidlee/.dotfiles/scripts/better-branch.sh
# fza = "!git ls-files -m -o --exclude-standard | fzf -m --print0 | xargs -0 git add"

# these other mods also don't work since I don't know what diff-so-fancy is
#[core]
# pager = diff-so-fancy | less --tabs=4 -RF
#
#[interactive]
# diffFilter = diff-so-fancy --patch
```

test with:
* git graph
    - add --all to see all branches
* git ls
* git ll
* git lds
* git recent-branches
* git authors
* git search some_text
* git dl
    - shows last diff file list only
* git dlc
    - shows actual diff of last commit.  normal git diff shows current changes.
* git alias




