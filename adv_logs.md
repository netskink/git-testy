# Notes on making logs more visual

## Colorize Logs

```
git config --global color.ui true
```

## One line logs

This will show the log as commit hash and then commit message

```
git log --pretty=oneline
```

This is similar but its a shortened hash, author date, subject (commit message) and auther name

```
git log --pretty=format:"%h [%ad-] %s [%an]"
```

### Log print format notes

```
%ad author date
%an author name
%h SHA hash
%s subject
%d ref names
```

This will show the -Naur patch for each commit.

```
git log --oneline -p
```


This shows How many inserts and deletions per commit

```
git log --oneline --stat
```

## Visual Graph

Methods to show a visual branch of commits

### basic 1

```
git log --oneline --graph
```

### basic 2

Not even sure I can see a difference

```
git log --all --decorate --oneline --graph
```

### with more info

with oneline. the --date part argument is redundant.  The dates are omitted

```
git log --graph --abbrev-commit --decorate --date=relative --all
```

### complex versions from stack overflow which require an alias

```
git log --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(bold white)— %an%C(reset)%C(bold yellow)%d%C(reset)' --abbrev-commit --date=relative
```

and

```
git log --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(bold white)— %an%C(reset)' --abbrev-commit
```

and

```
git log --graph --date-order --date=short --pretty=format:'%C(auto)%h%d %C(reset)%s %C(bold blue)%ce %C(reset)%C(green)%cr (%cd)'
```

## Example using an alias

Create an alias

```
git config --global alias.hist "log --graph --date-order --date=short \
--pretty=format:'%C(auto)%h%d %C(reset)%s %C(bold blue)%ce %C(reset)%C(green)%cr (%cd)'"
```

### Usage

* `git hist` - Show the history of current branch

* `git hist --all` - Show the graph of all branches (including remotes)

* `git hist master devel` - Show the relationship between two or more branches

* `git hist --branches` - Show all local branches

Add --topo-order to sort commits topologically, instead of by date (default in this alias)





