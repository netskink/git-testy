# rebase

rebase can be used to squash commits and rewrite commit messages.

## rewrite commit messages

Say for instance, you want to rewrite a commit message
changing "wip" to "edited the basics.md file"
for a commit at `HEAD~8`.


```
$ git log --oneline
ce894e9 (HEAD -> master, origin/master, origin/HEAD) added some notes about squash
7d2376b git show and remove from history
883ee4e Update basics.md
3853fe2 wip
0c69aac wip
72a74e0 wip
be75e7c wip
8e7b367 wip
d59c4e8 wip                     <----- Rewrite this one to provide detail
126ceab Update basics.md
```

What does this particular commit do?

```
$ git show HEAD~8
commit d59c4e8f1f6546830801f85df63b0f7128e55446
Author: John F. Davis <davisjf@gmail.com>
Date:   Thu Jul 16 10:55:35 2020 -0400

    wip

diff --git a/basics.md b/basics.md
index c78143c..a2ed39c 100644
--- a/basics.md
+++ b/basics.md
@@ -48,7 +48,7 @@ Host github.com-company                      <-----+
     # git@github.com-company:Company/some_repo.git |
                                                    |
 # default github                             <-----+
-Host github                                  <-----+
+Host github.com                              <-----+
     HostName github.com
     User git
     IdentityFile ~/.ssh/id_rsa
```

It modifies the basics.md file.  Here is how to rewrite
this commit message.


```
$ git  rebase -i HEAD~9
```

This opens the commit  rebase editor.  The commit in question 
will be at the top of the list.  Note, the commit examined
via show is `HEAD~8`, but the rebase command uses `HEAD~9`.

```
pick d59c4e8 wip
pick 8e7b367 wip
pick be75e7c wip
pick 72a74e0 wip
pick 0c69aac wip
pick 3853fe2 wip
pick 883ee4e Update basics.md
pick 7d2376b git show and remove from history
pick ce894e9 added some notes about squash
```

Since we want to rewrite the commit message and not
squash the commit, use the `reword` option in the
rebase editor.

```
reword d59c4e8 wip
pick 8e7b367 wip
pick be75e7c wip
pick 72a74e0 wip
pick 0c69aac wip
pick 3853fe2 wip
pick 883ee4e Update basics.md
pick 7d2376b git show and remove from history
pick ce894e9 added some notes about squash
```

Write - quit the rebase editor to save the operation and
the commit rewrite message editor comes up.

```
wip

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Author:    John F. Davis <davisjf@gmail.com>
# Date:      Thu Jul 16 10:55:35 2020 -0400
#
# interactive rebase in progress; onto 126ceab
# Last command done (1 command done):
```

Change the existing commit history line to be more specific

```
Modified the basics.md file

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Author:    John F. Davis <davisjf@gmail.com>
# Date:      Thu Jul 16 10:55:35 2020 -0400
#
# interactive rebase in progress; onto 126ceab
```

Rewrite the change log in the remote with a force push.
Notice the commit hash has changed. Also notice that
all the subsquent hashes have changed. This is why people
warn about using this option. 

```
$ git push -f
$ git log --oneline
36943c6 (HEAD -> master, origin/master, origin/HEAD) added some notes about squash  <--- hash delta
14e2ee2 git show and remove from history    <--- commit hash has changed
909a1e9 Update basics.md                    <--- commit hash has changed
59cbd5a wip                                 <--- commit hash has changed
ac71ab1 wip                                 <--- commit hash has changed
574b69a wip                                 <--- commit hash has changed
b2dd174 wip                                 <--- commit hash has changed
d032462 wip                                 <--- commit hash has changed
5a1bf4f Modified the basics.md file         <--- notice commit message and hash changed
126ceab Update basics.md                    <--- commit hash unchanged
```

A comparision of before and after

```
$ git log --oneline

BEFORE                                    AFTER
-------------------------------------------------------------------
ce894e9 (HEAD                             36943c6 (HEAD -> master, origin/master, o
7d2376b git show and remove from history  14e2ee2 git show and remove from history 
883ee4e Update basics.md                  909a1e9 Update basics.md                 
3853fe2 wip                               59cbd5a wip   
0c69aac wip                               ac71ab1 wip 
72a74e0 wip                               574b69a wip 
be75e7c wip                               b2dd174 wip                              
8e7b367 wip                               d032462 wip                              
d59c4e8 wip                               5a1bf4f Modified the basics.md file 
126ceab Update basics.md                  126ceab Update basics.md 
```                                     



