# commit

## Ammend last commit

This adds new stages to the most recent commit.  Its so that you can modify your last commit.  You
can change your log message and the files that appear in the commit.


```
$ git commit --ammend -m 'existing message'
```

## reuse last commit message

This is helpful when working on code that others review.  It allows
a new change to an existing commit to reuse the same commit message.
```
git commit --reuse-message=HEAD
```

