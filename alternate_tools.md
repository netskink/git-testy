# Alternate tools for work with git

These are some tools for working with git.  YMMV


## tig

An ncurses UI for git

## gitkraken

A cross platform GUI for git

## git-wft

A opensource collection of ruby scripts

It is [here.](https://github.com/DanielVartanov/willgit)

## ungit

GCP datalab notebooks use this for using git.

It is [here.](https://github.com/FredrikNoren/ungit)

## Jetbrains

pycharm, cstorm? etc have a visual git tool

## visual studio

It has a visual GUI



