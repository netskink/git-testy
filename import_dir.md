# import into github an existing directory

## setup in github


1. in github create a new repo
2. Do no create a README or .gitignore

## workflow on local computer

Once the github repo is created, switch back to the directory
you want in github.

1. change directory to folder of interest
2. `$ git init`
3. create a .gitignore
4. `$ git add --all`
5. `$ git commit -m "initial commit"
6. `$ git remote add origin git@github.com:<userid>/<git repo>.git`
7. `$ git push -u origin master`


