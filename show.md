# show

Do this to see a file at a particular time.

```
$ git show <hash>:path/to/file
```


## showing a summary of changes

```
$ git show --stat HEAD^
$ git show --name-only HEAD^
$ git show --name-status HEAD^
$ git show --stat --oneline HEAD^
$ git show --stat --oneline HEAD^^..HEAD
$ git show --stat --oneline HEAD~4..HEAD
$ git show --stat --oneline HEAD~4..HEAD
 ```


