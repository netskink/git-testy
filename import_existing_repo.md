# importing an existing git repo

Let's say you have an existing git repo, and you want to create
a new repo on github/gitlab and preserve the existing git commits.

1. git clone the desired repo
2. create a repot in github/gitlab
3.  Grab the remotes existing repot command from the webpage
    * git remote add origin git@github.com:netskink/somerepo.git
4. Using the URL above, modify the upstream url
    * `git remote set-url origin git@github.com:netskink/somerepo.git`
5. Push to remote. Specify branch as well
    * `git push origin HEAD:refs/heads/main`
    * or to dev branch
    * `git push origin HEAD:refs/heads/dev`




