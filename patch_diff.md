# Patch and Diff notes

Not really specific to git, but will help you understand how to diff and patch a filesystem.

## Make patch of two directories syntax

Given a directory layout like this:

```
   somedir/
      |
      +--dir1/
      |
      +--dir2/
```

Where `dir1` and `dir2` are two similar directories.


Do this to make a patch file which describes the changes
to make `dir1` into `dir2`.

From the root dir `somedir` which contains the two dirs.

```
somepath/somedir$ diff -Naur --exclude=.git dir1 dir2 > dir1dir2.patch
```


## Test patch file syntax

Use this command to test to see if patch works correctly.  It will not modify any files.

```
somepath/somedir$ patch --dry-run -p0 < dir1dir2.patch
```

## Apply patch file syntax

Use this command to make dir1 look like dir2.

```
somepath/somedir$ patch -p0 < dir1dir2.patch
```


