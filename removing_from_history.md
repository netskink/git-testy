# Removing lines of text from git history

## Using `git-filter-branch`

This method suggests using a newer method

```
WARNING: git-filter-branch has a glut of gotchas generating mangled history
         rewrites.  Hit Ctrl-C before proceeding to abort, then use an
         alternative filtering tool such as 'git filter-repo'
         (https://github.com/newren/git-filter-repo/) instead.  See the
         filter-branch manual page for more details; to squelch this warning,
         set FILTER_BRANCH_SQUELCH_WARNING=1.
Proceeding with filter-branch...
```


Find the file which has the problem

```
$ git log -S "an_api_key" main --name-only --pretty=format: | sort -u
```

Replace the test in history 

NOTE: Run this command from the top of the git repo.
```
$ git filter-branch --tree-filter "if [ -f somedir/somefile ]; then sed -i s/an_api_key/APIKEYHERE/g  somedir/somefile;fi"
```

Force push to remote

```
$ git push origin -f main:main
```


## Alternative method

### remove an entrie file from git history

This didn't work for me.  It blew away my remote.  With that said, I
just deleted the git repo and reuploaded a new one.  Not to mention
I had to download install a python script to do this.  Here are
the two approaches for removing an entire file.

```
$ git filter-branch --index-filter 'git rm --cache <file>' HEAD
```


