# Glossary

## HEAD

HEAD is a symbol referring to the tip of a branch.  Its shorthand for the latest commit 
hash.

```
$ git log --oneline
98abb29 (HEAD -> master, origin/master, origin/HEAD) added log
36b28c8 add rebase.md
6db3b76 added some notes about squash
d45faa0 git show and remove from history
82d2f8a Update basics.md
7605d56 gitblame gitignore import mods
1e6b4e4 update removing_from_history
c20ee6b credential helpers mod
a40bdce modified basics.md
be4ef03 Added remove.md
5a1bf4f Modified the basics.md file
126ceab Update basics.md
```


## ^

Offset symbol. 

* HEAD^ commit one previous from HEAD
* HEAD^^ commit two previous from HEAD
* HEAD^^^ commit three previous from HEAD
* etc


## ~

Offset symbol.

Similar to `^` but allows numeric indexes.

Note, there is some difference with merging that i'm unfamilar with.

* HEAD~` commit one previous from HEAD
* HEAD~2 and HEAD^^ are equivalent



