# Notes on `.gitignore`

## official doc url

See [here](https://git-scm.com/docs/gitignore)

## use of `*` vs `**`

In the dir where the `.gitignore` exists, a trailing `**` matches all content.

This would ignore anything in subdir foo.
```
foo\**
```


