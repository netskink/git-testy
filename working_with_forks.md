# Note on working with forks

## Using github/gitlab to maintain sync

1. visit your forks webpages
2. click the `sync fork` button
    * Now, your github repo version is up to date with the original.
3. in your git repo, do:
```
$ git pull
```
    * Now, your local fork repo up to date with the original.


## Using the command line to maintain sync

1. clone your forked repo

```
$ git clone whatever
```

* whatever is the https or ssh git repo URI

2. Add the upstream repo

```
$ git remote add upstream whatever
```

* upstream is the alias for whatever
* whatever is the https or ssh git repo URI

3. Sync your local repo to upstream/original repo

```
$ git fetch upstream
```

4. checout your forks local master branch

```
$ git checkout master
```

5. Merge the upstream master branch

This merges changes from the upstream master
branch into your local forked master branch.
The result is that your fork's master branch
is in sync with the upstream repo without
losing your local changes.

```
$ git merge upstream/master
```

6. Push the changes to your fork on github/gitlab

```
$ git push origin master
```


