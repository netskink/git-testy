# git cache usernames and passwords for https git clone repo

This shows how to cache username and passwords for git clone of repo's using https.  Note: this
will cache the password in clear text.  Add method for encrypting the https username/password pair.


## Basic operation

This will cache usernames and password for subsequent git operations after the first username/pasword entry

```
git config credential.helper store
git config credential.helper cache
```

# this will cache credentials for a few hours
git config --global credential.helper 'cache --timeout=10800'

