# Handling Git Conflicts



## How to handle git conflicts.

ie. a git merge problem

### Method 1

*Using 2 directories and Meld
*dir1: working dir
*dir2: copy of working dir


We did the following in the working dir:

* modified a file
* Added git added the file
* git committed the file
* git push
* we encountered a problem

We copied dir1 to dir2 to save our changes

We changed path to dir1 and cleaned up git to synch with HEAD

we tried to git stash the changes -- this did not work

so, we did the following
```
git merge --abort
git reset --soft HEAD^
git stash push
git pull
```
 
This made the tree up-to-date with the repo
We used Meld to merge the changes.


### Method 2

```
git config merge.tool vimdiff
git config merge.conflictstyle diff3
git config mergetool.prompt false
git mergetool
git stash pop
git mergetool
```

Then we used Meld to merge the changes.

### Method 2b

TODO: Add section on how to use vimdiff as a git mergetool













