# stash

## save files as you switch between branches

```
$ git stash
```

## apply changes from stash

```
$ git stash apply
```

## tossing stash

```
$ git stash pop
```

## List stashes

```
$ git stash list
```

## Show a particular stash

```
$ git stash show 1
```


