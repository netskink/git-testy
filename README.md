# git-testy

This is a sample repo for learning how to use git.  Created 20200308

# TODO
These are the topics for the next class

### High priority
* How to setup ssh keys and passphrase so they are cached on linux, osx and windows
* How to import existing code into a git repo
* Demonstrate a simple git branching strategy

### Low priority
* How to perform a rebase operation to collapse git commits
* How to fork a repo
* How to perform a git pull request


