# Markdown Basics

These are notes on how to use Markdown to create
README.md files.

## Tools for different OS installations

I prefer the side by side Markdown editors.  All of these editors support live side-by-side editing.

### OSX
For OSX macdown is a good tool.  It has the following helpful features

* syntax highlighting
* github style guides
* spellcheck


Usage: 

`macdown README.md`

### Linux

Multiple choices exist for Linux.  By default, these two are avaialbe via Debian repo's.

* ghostwriter 

    Has the following features
    * export to PDF and DOC
    * spellcheck
   

* formiko

    Has the following features
    * export to ePub
    * periodic saving
    * vim mode
    * spellcheck


### Windows

* ghostwriter


## Markdown syntax

Markdown uses a somewhat human readable text syntax to create formatted HTML when rendered by a markdown parser.

* Use a blank line to separate lines blocks of text.  Otherwise text will reflow to form a single paragraph.

```
    This will be rendered on one line.
    This line will be rendered on the same line.

    This line will be started on a new line.

```

* Use \# to create a heading.  Use \#\# to create a subheading and so forth.


* Use a single back-tick to enclose in context code blocks. 

```
    This `word` will be rendered in code style.

```

* Use three back-ticks before and after a code block to render a code block using existing line endings.

```
   ```
   some code
   some more code
   yet more code
   ```
```


* Escape markdown operators with a backslash.
```
    The will show \#\# two ##
```

* mods for text

```
   **Bold Text**
   *Italic Text*
   --- horizontal line
```

* Embedding a URL Link

```
link [title](http://some.url)
```

* Embedding an image

```
image ![alt text](image.png)
```

Example of using the image tag.

![gitar_hero_image](pics/gitar_hero.png)

Example of using the image tag to show a file from github

![alt text](https://raw.githubusercontent.com/netskink/public-pastebin/master/img/demoSM.png)


* Embedding a Table


You can align text in the columns to the left, right, or center by adding a colon (:) to the left, right, or on both side of the hyphens within the header row.

| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |



