# git log notes




## log

```
$ git log
```

Shows a list of commits with the most current at the top
and the oldest at the bottom.  For each commit 
entry a brief summary is provided.  The summary shows:

* commit hash 
    - a unique hex id
* author
    - who made the commit
* date 
    - when the commit was made
* comment
    - The message from `commit add -m "some comment about this mod"


### Example:

```
commit ce894e993e8f470edf8f61548fe120fc392bd68c (HEAD -> master, origin/master, origin/HEAD)
Author: John F. Davis <davisjf@gmail.com>
Date:   Fri Nov 10 08:20:59 2023 -0500

    added some notes about squash

commit 7d2376b00ff3af8cb4a9ab4627327ba55bb5d69c
Author: John F. Davis <davis@zatoichi.jfdhome.net>
Date:   Sun Apr 9 10:58:53 2023 -0400

    git show and remove from history

commit 883ee4ebfd3deeb744eb9407de5f9fc7af54311d
Author: John F. Davis <davisjf@gmail.com>
Date:   Wed Apr 5 19:39:11 2023 +0000

    Update basics.md
````


## log --oneline

```
$ git log --oneline
```

This will make a brief list of commits and only the leading
six characters of the has is shown.

### Example

```
ce894e9 (HEAD -> master, origin/master, origin/HEAD) added some notes about squash
7d2376b git show and remove from history
883ee4e Update basics.md
```



