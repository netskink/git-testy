# push

# git push

git push <repository>

if <repository> is not specified,
branch.*.remote for current branch is consulted

# git push --set-upstream xxx


xxx is a name of a remote.


Note, if you are working on a branch, you don't have to specify the branch.

As an example consider this workflow where I am working on a patch to my repo
to push to a fork.

```
# Switched to a new branch 'add_gn'
$ git checkout -b add_gn
# Add a file to commit
$ git add docs/api/zopen_releases.json
# commit a file to the custom branch
$ git commit -m "Add gnport to list of packages for depot_toolsport"
# add the remote, in this case its my fork
$ git remote add mine git@github.com:netskink/meta.git
# push without specifying the branch, its inferred.
$ git push --set-upstream mine
# For instance this would have worked as well.
# git push --set-upstream mine add_gn
```

Afterwards, work on this branch can just do:

```
$ git push
```



