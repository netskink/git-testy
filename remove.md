# remove

If you have a file you want to remove from git including history.  Issue this comamnd in root of repo.

```
git filter-branch --index-filter "git rm -rf --cached --ignore-unmatch <path_to_file>" HEAD
```
