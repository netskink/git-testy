# Checkout

# combo command to create a branch and checkout

```
$ git checkout -b branch_name
```

# discard changes to a file and revert back to HEAD

```
$ git checkout -- filename
```
