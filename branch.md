# branch commands

## delete a branch

```
$ git branch -d branch_name
```


## force delete a branch

No merge check

```
$ git branch -D branch_name
```

## rename a branch

```
$ git branch -m existing_branch_name new_branch_name
```

## rename existing branch you are on at the moment

```
$ git branch -m new_branch_name
```




