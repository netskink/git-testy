# remote


## git remote

This is the "server" where local git repo is maintained remotely.

This command displays the various remotes.

```
$ git remote
origin
upstream
```

Using -v shows the urls assocated with names.

```
$ git remote -v
origin	git@github.com:netskink/meta.git (fetch)
origin	git@github.com:netskink/meta.git (push)
upstream	git@github.com:ZOSOpenTools/meta.git (fetch)
upstream	git@github.com:ZOSOpenTools/meta.git (push)
```

## git remote add

Remote add allows a secondary remote to be added.  In this
case, `upstream` is the name associated with the repo. It
allows a shorthand for the long server URI?

```
$ git remote add upstream git@github.com:ZOSOpenTools/meta.git
```

or 

```
$ git remote add mine git@github.com:netskink/meta.git
```

See `git push for more info` about how to use the remote
afterwards.



