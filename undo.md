# Methods to undo changes

Taken from this page
https://docs.gitlab.com/ee/topics/git/numerous_undo_possibilities_in_git/#undo-local-changes


## methods for local changes which have not been staged or pushed to server

### Discard all local changes, but save them for possible re-use later:

```
git stash
```

### Discarding local changes (permanently) to a file:

```
git checkout -- <file>
```

### Discard all local changes to all files permanently:

```
git reset --hard
```


