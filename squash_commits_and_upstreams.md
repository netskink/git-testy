# Squashing commits

These are notes which come from working with upstream github repos.
I don't really know much about merge and rebase.  I have a brief knowledge
but this is how I would sum it up.

* merge is when you have one branch and you want to combine it
with another branch.  In this case its considered merging.  The
basic technique is to checkout the branch you want to have 
final changes and then use the merge command to specify the source
branch for changes to merge into the current branch.

* rebase is when you want to take a series of changes and streamline
them into one commit.  I have also used this to remove api keys
from git history.

These notes came about, because my goal was to rewrite commits 
so that three commits became one.
I've got some notes on this regarding upstream work.
I've also made some attempts which so far have failed in that regard.


## Notes from people on discord regarding my fork and merge problem

These are some comments I found when asking about it via discord.
For now, they are just comments to revisit later.


When doing development with a fork, don't think about syncing the work with fork, that's the wrong flow. It's much better if you set the upstream to be the upstream:
git remote add upstream "ssh://.... upstream-url"
git branch -u upstream/main


Then you can do
git pull --rebase


Forget about your fork branches other than the one you are doing development on.


also:

rebase? https://docs.gitlab.com/ee/topics/git/git_rebase.html#regular-rebase




## Approach #1

Let's say you have 3 commits and you want to collapse them into one commit.


List the last three commits with just one line


```
git log --oneline -n 3
a2373ec (HEAD -> main, origin/main, origin/HEAD) add line 3
6f7f2a2 add line 2
63eb32e add line 1
```
Start the rebase

* count the number of commits in this case, 3

```
git rebase -i HEAD~3
```

* Pick the first commit I did, leave the prefix as pick
* Modify all the commits I did as squash
* Save the rebase

The commit summary will appear.  Rewrite so you have just
one commit and then delete the others.

Something about detached head.

* do a new git pull

Do a git push, on the message, change to your summary again.

Now, git log will show:

```
57f5d61 (HEAD -> main, origin/main, origin/HEAD) Add three lines a, b, c
103e181 add lines A-C
8299191 add line C
5028f79 add line B
52d3d0d add line A
```

This was done with pull.rebase=false

Deleted the pull.rebase setting from ~/.gitconfig

Redid the above and now we have:

```
80e070e (HEAD -> main, origin/main, origin/HEAD) add line Z
f535aaa add line Y
d8b697b add line X
```

Attempt the rebase again with goal of squashing the X,Y, Z commits

```
git rebase -i HEAD~3
```



When doing development with a fork, don't think about syncing the work with fork, that's the wrong flow. It's much better if you set the upstream to be the upstream:
```
git remote add upstream "ssh://.... upstream-url"
git branch -u upstream/main
```

Then you can do
```
git pull --rebase
```

Afterwards, we ahve a detached HEAD.  Notice the change in branch color for our git tool.

```
git push
```

Get the message about detached. It fails with this message:

```
 davis@doom  ~/progs/github/gurpsv1  ⇅ main  git push
To github.com:netskink/gurpsv1.git
 ! [rejected]        main -> main (non-fast-forward)
error: failed to push some refs to 'github.com:netskink/gurpsv1.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

The log shows just the single squashed commit A,B,C.  The individual ones
are not there.

Attempt the git pull

```
 davis@doom  ~/progs/github/gurpsv1  ⇅ main  git pull
hint: You have divergent branches and need to specify how to reconcile them.
hint: You can do so by running one of the following commands sometime before
hint: your next pull:
hint:
hint:   git config pull.rebase false  # merge
hint:   git config pull.rebase true   # rebase
hint:   git config pull.ff only       # fast-forward only
hint:
hint: You can replace "git config" with "git config --global" to set a default
hint: preference for all repositories. You can also pass --rebase, --no-rebase,
hint: or --ff-only on the command line to override the configured default per
hint: invocation.
```

Do the rebase setting

```
git config pull.rebase true
```

Do the git pull again

```
 davis@doom  ~/progs/github/gurpsv1  ⇅ main  git pull
dropping 8bf96b3823bd3126f837f3c56871602f5f3ac0ae add lines X,Y,Z -- patch contents already upstream
```

Do the git log again

```
80e070e (HEAD -> main, origin/main, origin/HEAD) add line Z
f535aaa add line Y
d8b697b add line X
```


It did not squash.  Looking at the global config and the local config, here are the settings:

```
davis@doom  ~/progs/github/gurpsv1   main  cat ~/.gitconfig | grep rebase
 ✘ davis@doom  ~/progs/github/gurpsv1   main  cat .git/config | grep rebase
	rebase = true
```

This makes sense. the config was only for the repo and nothing is in the global.


Change the setting to false

```
 davis@doom  ~/progs/github/gurpsv1   main  git config pull.rebase false
 davis@doom  ~/progs/github/gurpsv1   main  cat .git/config | grep rebase
	rebase = false
```

Do again with changes alpha, beta, delta

After we do the pull, it does the merge error again.  I rewite the line so now we have two

```
commit b5f31d47ab88c102b5307a686fc50cf412cad9ee (HEAD -> main)
Merge: a61ee37 43f13d0
Author: John F. Davis <davisjf@gmail.com>
Date:   Thu Oct 19 11:43:37 2023 -0400

    add three lines as part of merge

commit a61ee37f94954f218cf497b66c15c9534ccfffad
Author: John F. Davis <davisjf@gmail.com>
Date:   Thu Oct 19 11:30:53 2023 -0400

    add lines alpha,beta,delta
```

Do the git push

```
 davis@doom  ~/progs/github/gurpsv1  ↱ main  git push
Enumerating objects: 2, done.
Counting objects: 100% (2/2), done.
Delta compression using up to 10 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 342 bytes | 114.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), done.
To github.com:netskink/gurpsv1.git
   43f13d0..b5f31d4  main -> main

```


### Summary

So apparantly I can not do this.  Reading up on the subject, it might
be due to the fact that I have one branch.  Let's try to have a different
branch and then merge into the main branch.

### Using a dev branch and then merging into main branch



1. Create a new branch

```
git branch dev
```

2. Switch to dev branch

```
git checkout dev
```
go ahead and push to set remote

```
git push --set-upstream origin dev
```


At this point my git prompt mod, switches to dev and I get a message
which says:

```
Switched to branch 'dev'
 davis@doom  ~/progs/github/gurpsv1   dev 
```

Do the same thing as before

```
10123  vi README.md
10124  git add README.md; git commit -m "add line line 01"; git push
10125  git push --set-upstream origin dev
10126  vi README.md
10127  git add README.md; git commit -m "add line line 01"; git push
10128  vi README.md
10129  git add README.md; git commit -m "add line line 03"; git push
```

Show the log

```
49dafd7 (HEAD -> dev, origin/dev) add line line 03
4a981f5 add line line 01
ddef061 add line line 01
b5f31d4 (origin/main, origin/HEAD, main) add three lines as part of merge
a61ee37 add lines alpha,beta,delta
43f13d0 add line delta
99e8e02 add line beta
b102bb1 add line alpha
80e070e add line Z
f535aaa add line Y
d8b697b add line X
57f5d61 Add three lines a, b, c
103e181 add lines A-C
8299191 add line C
5028f79 add line B
52d3d0d add line A
8be3c6f have a rudimentary file io and combat queue testing
7f5991a applying stash
a2373ec add line 3
6f7f2a2 add line 2
63eb32e add line 1
5e70939 Squash test
cc57204 Test of rebase
```

Do the git rebase and notice the order of the commits. Its reverse

```
git rebase -i HEAD~3
```

result 1

```
pick ddef061 add line line 01
pick 4a981f5 add line line 01
pick 49dafd7 add line line 03
```

result 2

```
pick  ddef061 add line line 01
squash 4a981f5 add line line 01
squash 49dafd7 add line line 03
```

do the :wq

result 3

```
# This is a combination of 3 commits.
# This is the 1st commit message:

add line line 01

# This is the commit message #2:

add line line 01

# This is the commit message #3:

add line line 03
```

result 4

```
# This is a combination of 3 commits.
# This is the 1st commit message:

add lines 01-03
```

This results in a detached head again.

git pull

```
 ✘ davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ dev  git pull
Merge made by the 'ort' strategy.
```

git log --oneline

```
283e451 (HEAD -> dev) Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 (origin/dev) add line line 03
4a981f5 add line line 01
ddef061 add line line 01
```

Switch to main branch, we want to merge into there

```
git checkout main
```

Git merge

```
davis@QGVCTV2F5Y  ~/progs/github/gurpsv1   main  git merge dev
Updating b5f31d4..283e451
Fast-forward
 README.md | 4 ++++
 1 file changed, 4 insertions(+)
```

git log

```
283e451 (HEAD -> main, origin/dev, dev) Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 add line line 03
4a981f5 add line line 01
ddef061 add line line 01
```

git push and then git log shows the same thing as before

```

283e451 (HEAD -> main, origin/main, origin/dev, origin/HEAD, dev) Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 add line line 03
4a981f5 add line line 01
ddef061 add line line 01
```

During this process, git config for merge was set to pull.rebase=false

```
davis@QGVCTV2F5Y  ~/progs/github/gurpsv1   main  git config --list | grep rebase
pull.rebase=false
```



# Approach #3

Change the `pull.rebase=false` setting to `true`

```
git config pull.rebase true
```

View the change

```
git config pull.rebase
true
```

Add all the lines again and push each time.

```
8775f70 (HEAD -> dev, origin/dev) Zeile drei
7ae4499 Zeile zwei
e124474 Zeile eins
```



Do the git rebase and notice the order of the commits. Its reverse

```
git rebase -i HEAD~3
```

result 1

```
pick e124474 Zeile eins   <-- This was the first one done in time
pick 7ae4499 Zeile zwei
pick 8775f70 Zeile drei   <-- This was the last one done in time

# Rebase fef03c6..8775f70 onto fef03c6 (3 commands)
```

I notice the text in this file says these lines can be re-ordered.
Do as before, and use this setting to maintain one variable at a time.

Also, if I pick the first one as squash, vim will highlight this in red.

result 2

```
pick  e124474 Zeile eins
squash 7ae4499 Zeile zwei
squash 8775f70 Zeile drei
```

result 3

```
39581c0 (HEAD -> dev) Add Zeile eins zwei drei
fef03c6 added some urls
c122b36 wip
4fc11e3 wip
75f4fb8 (origin/main, origin/HEAD, main) wip
283e451 Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 add line line 03
4a981f5 add line line 01
```

This looks correct.  It has squashed the commits as desired.  


Switch to main branch, we want to merge into there

```
git checkout main
```

Do the Git merge from dev

```
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1   main  git merge dev
Updating 75f4fb8..39581c0
Fast-forward
 README.md                       |  11 +++++
 TestGurps/TestCombat.swift      |  19 ++++++++
 gurps.xcodeproj/project.pbxproj |  20 ++++++---
 gurps/basicCombat.swift         | 213 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 gurps/combat.swift              | 248 -----------------------------------------------------------------------------------------------------
 gurps/fileIO.swift              | 155 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-
 gurps/moveSM.swift              |  10 +++++
 gurps/notes.md                  |   3 ++
 gurps/people.swift              |   1 -
 moves.txt                       |   9 ++++
 10 files changed, 433 insertions(+), 256 deletions(-)
 create mode 100644 gurps/basicCombat.swift
 delete mode 100644 gurps/combat.swift
 create mode 100644 gurps/moveSM.swift
 create mode 100644 moves.txt
```

git log

```
39581c0 (HEAD -> main, dev) Add Zeile eins zwei drei
fef03c6 added some urls
c122b36 wip
4fc11e3 wip
75f4fb8 (origin/main, origin/HEAD) wip
283e451 Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 add line line 03
```


git push

```
davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ↱ main  git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 10 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 351 bytes | 351.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (2/2), completed with 2 local objects.
To github.com:netskink/gurpsv1.git
   75f4fb8..39581c0  main -> main
```

Wow, this worked.  It did not ask me to merge and it actually squashed the commits.
Here is the final git log

```
39581c0 (HEAD -> main, origin/main, origin/HEAD, dev) Add Zeile eins zwei drei
fef03c6 added some urls
c122b36 wip
4fc11e3 wip
75f4fb8 wip
283e451 Merge of squashed commit
95a8c26 add lines 01-03
49dafd7 add line line 03
4a981f5 add line line 01
ddef061 add line line 01
```


## One more time on main branch 

Clean out all the line adds and repeat process.  Add one line each time,
commit and push.  Verify the log in remote has three individual commits.

```
bb8bd29 (HEAD -> main, origin/main, origin/HEAD) added line three
ed70da7 added line one
dc24349 added line one
```
Do the rebase again

```
git rebase -i HEAD~3
```

result 1

```
pick dc24349 added line one
squash ed70da7 added line one
squash bb8bd29 added line three
```

result 2

```
added lines one - three
```

git log again

```
24e7918 (HEAD -> main) added lines one - three
192c6ef remove all those test lines
```

git push and it will say its rejected

```
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push
To github.com:netskink/gurpsv1.git
 ! [rejected]        main -> main (non-fast-forward)
error: failed to push some refs to 'github.com:netskink/gurpsv1.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

git pull

```
 ✘ davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git pull
dropping 24e7918e406ba632e4c16924a2de40d3b2060b29 added lines one - three -- patch contents already upstream
Successfully rebased and updated refs/heads/main.
```

git push again and it does not pop up the merge bit

```
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1   main  git push
Everything up-to-date
```

however, the log does not squash the commits

```
bb8bd29 (HEAD -> main, origin/main, origin/HEAD) added line three
ed70da7 added line one
dc24349 added line one
```

Repeat again and this time do a force push 

```
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push
To github.com:netskink/gurpsv1.git
 ! [rejected]        main -> main (non-fast-forward)
error: failed to push some refs to 'github.com:netskink/gurpsv1.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
 ✘ davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push --help
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push --help
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git log
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push --help
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1  ⇅ main  git push -f
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 10 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 352 bytes | 70.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (2/2), completed with 2 local objects.
To github.com:netskink/gurpsv1.git
 + bb8bd29...5cbad47 main -> main (forced update)
 davis@QGVCTV2F5Y  ~/progs/github/gurpsv1   main 
```

The resultant git log

```
5cbad47 (HEAD -> main, origin/main, origin/HEAD) added line one - three
192c6ef remove all those test lines
39581c0 (dev) Add Zeile eins zwei drei
fef03c6 added some urls
c122b36 wip
4fc11e3 wip
```

## Working with Igor

```
git log --author=davis --oneline
```

Command done with Igor

```
git rebase origin/main
```

Some of the command which kind of correlate with 
the aws comments are:

```
git remote add upstream git@github.com:ZOSOpenTools/meta.git
```

This adds a new upstream which refers to the source
of the fork.

```
git fetch upstream
```

Syncs my fork with the upstream.



```
  563  cp QuickStart.md QuickStart.md.arc
  564  git checkout QuickStart.md
  565  diff QuickStart.md QuickStart.md.arc
  566  git reset --hard origin main
  567  git reset --hard origin/main
  568  diff QuickStart.md QuickStart.md.arc
  569  ls
  570  git remote -b
  571  git remote -v
  572  git remote add upstream git@github.com:ZOSOpenTools/meta.git
  573  git remote -v
  574  git fetch upstream
  575  git reset --hard upstream/main
  576  diff QuickStart.md QuickStart.md.arc
  577  cp QuickStart.md.arc QuickStart.md
  578  git add QuickStart.md
  579  git commit -m "clean merge of Quickstart"
  580  git push origin/dev
  581  git push origin dev
  582  git push -f origin dev
  583  git checkout main
  584  git checkout dev
  585  git push origin:main
  586  git checkout main
  587  git reset --hard origin/dev
  588  git push -f origin main

```

In the end, the key was using the remote to the main repo where I do the fork.

